import Meta from "../components/meta";
import Footer from "../components/footer";
import FormHeader from "../components/form-header";
import FormBody from "../components/form-body";

export default function Form() {

  return (
    <>
      <Meta />
      <FormHeader />
      <FormBody />
      <Footer />
    </>
  );
}
